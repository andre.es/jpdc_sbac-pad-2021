#!/usr/bin/env python3

#script to parse SLURM sacct output and generate a sqlite data base

#modules import list


import os
import subprocess
import tempfile
import argparse
import datetime
import sys
import pandas as pd

## Functions


# Function that write a python list into a csv file
# creation date: 2020-10-30
# Author: Andre R. Carneiro
# in: filename, input python list, list of columns
# out: operation status



def write_csv_dataset(fileName, inputLst, colNames):
	outMsg=""
	try:
		dataset= pd.DataFrame(inputLst, columns=colNames )
		dataset.to_csv(fileName, index=False, sep='|')
	except Exception as E:
		outMsg= str(E)
		return outMsg
	return {}


##

# Function consult sacct job list and generate list with the values
# creation date: 2020-10-27
# Author: Andre R. Carneiro
# in: start date, end date, project+knowledge area file name
# out: python list of jobs


def exec_sacct(startDate, endDate, KAFile, execFile):
	outMsg= ""
	#dictionary with applications names
	appNames= dict([
		#Batch run. Job only have the batch jobstep, which is the execution of the main slurm submission script
		("unknown", ["batch"]),
		
		#Python
		("Python", ["python","python3","python3.7","python3.8","python3.9","python2","python2.7"]),
		
		#Bash Script
		("Bash Script", ["bash"]),
		
		#Start with OpenMPI mpiexec
		("OpenMPI mpiexec", ["orted"]),
		
		#Start with MPICH/IntelMPI mpiexec
		("MPICH/IntelMPI mpiexec", ["hydra_bstrap_proxy","hydra_pmi_proxy","pmi_proxy"]),

		#VASP
		("VASP", ["vasp", "vasp_gpu", "vasp_std"]),

		#cp2k
		("CP2K", ["cp2k"]),

		#casino
		("CASINO", ["casino"]),

		#siesta
		("SIESTA", ["siesta"]),

		#gromacs
		("GROMACS", ["mdrun_mpi", "gmx_mpi", "gmx_mpi_gpu", "mdrun"]),

		#Quantum Espresso
		("QUANTUM ESPRESSO", ["a2y", "alpha2f.x", "average.x", "bands.x", "benchmark_libxc.x", "bgw2pw.x", "blc2wan.x", "bse_main.x", "casino2upf.x", "cell2ibrav.x", "cmplx_bands.x", "conductor.x", "cpmd2upf.x", "cppp.x", "cp.x", "current.x", "decay.x", "disentangle.x", "dist.x", "dos+want.x", "dos.x", "dynmat.x", "embed.x", "epa.x", "epsilon.x", "epw.x", "ev.x", "extract_core.x", "fd_ef.x", "fd_ifc.x", "fd.x", "fermi_proj.x", "fermi_velocity.x", "fftw-wisdom", "fftw-wisdom-to-conf", "fhi2upf.x", "fix_upf.x", "fpmd2upf.x", "fqha.x", "fs.x", "gcube2plt.x", "generate_rVV10_kernel_table.x", "generate_vdW_kernel_table.x", "gipaw.x", "gww_fit.x", "gww.x", "head.x", "hp.x", "ibrav2cell.x", "importexport_binary.x", "initial_state.x", "interpolate.x", "iotk", "iotk_print_kinds.x", "iotk.x", "kgrid.x", "kpoints.x", "lambda.x", "ld1.x", "manycp.x", "manypw.x", "matdyn.x", "midpoint.x", "molecularnexafs.x", "molecularpdos.x", "nc-config", "nccopy", "ncdump", "ncgen", "ncgen3", "ncpp2upf.x", "neb.x", "nf-config", "oldcp2upf.x", "open_grid.x", "p2y", "path_interpolation.x", "pawplot.x", "phcg.x", "ph.x", "plan_avg.x", "plotband.x", "plotproj.x", "plotrho.x", "plot.x", "pmw.x", "postw90.x", "ppacf.x", "pp.x", "projwfc.x", "pw2bgw.x", "pw2critic.x", "pw2gw.x", "pw2wannier90.x", "pw4gww.x", "pwcond.x", "pw_export.x", "pwi2xsf.x", "pw.x", "q2qstar.x", "q2r.x", "q2trans_fd.x", "q2trans.x", "read_upf_tofile.x", "rrkj2upf.x", "sax2qexml.x", "simple_bse.x", "simple_ip.x", "simple.x", "spectra_correction.x", "sumpdos.x", "sum_sgm.x", "turbo_davidson.x", "turbo_eels.x", "turbo_lanczos.x", "turbo_spectrum.x", "unfold.x", "upf2casino.x", "uspp2upf.x", "vdb2upf.x", "virtual_v2.x", "virtual.x", "wannier90.x", "wannier_ham.x", "wannier_plot.x", "wannier.x", "wfck2r.x", "wfdd.x", "wfk2etsf.x", "write_ham.x", "xc-info", "xc-threshold", "xspectra.x", "yambo", "yambo_kerr", "yambo_nl", "yambo_ph", "yambo_rt", "ypp", "ypp_nl", "ypp_ph", "ypp_rt"]),

		#siesta
		("ORCA", ["orca"]),

		#bie
		("BIE", ["bie"]),

		#namd
		("NAMD", ["namd", "namd2"]),

		#bagel
		("BAGEL", ["bagel"]),

		#beagle
		("BEAGLE", ["beagle"]),

		#fluka
		("FLUKA", ["fluka"]),

		#jaguar
		("JAGUAR", ["jaguar"]),

		#gaussian
		("GAUSSIAN", ["gaussian", "g09", "g16"]),

		#plumed
		("PLUMED", ["plumed"]),

		#schrodinger
		("SCHRODINGER", ["schrodinger"]),

		#amber
		("AMBER", ["amber", "pmemd.mpi", "pmemd.MPI", "pmemd", "pmemd.cuda", "pmemd.cuda.MPI"]),

		#galprop
		("GALPROP", ["galprop"]),

		#gamess
		("GAMESS", ["gamess"]),

		#hmmer
		("HMMER", ["hmmer"]),

		#lammps
		("LAMMPS", ["lammps","lmp"])
		])
	
	# set a SLURM environment variable to return dates in specific format
	os.environ['SLURM_TIME_FORMAT'] = '%Y-%m-%d %H:%M:%S'
	#List of jobs
	jobsLst= []
	
	#generate temporary files to store sacct output
	fout= tempfile.TemporaryFile()
	ferr= tempfile.TemporaryFile()

	#arguments used to query the slurm jobs' database
	args = ['sacct', '-a', '-P', '-o JobID,JobName,Account,Partition,NNodes,NCPUS,Start,End,Elapsed,State,NodeList', '-r cpu,cpu_long,cpu_scal,cpu_shared,cpu_small,nvidia,nvidia_dev,nvidia_long,nvidia_small,mesca2,dockthor,galphat,lnls,het_scal,meis2', '-n', '-T', '-X' ,'-D']
	
	if startDate:
		args.append(('-S '+startDate))
		
	if endDate:
		args.append(('-E '+endDate))
	
	try:
		p1= subprocess.Popen(args, stdout=subprocess.PIPE, env=os.environ)
	except Exception as E:
		outMsg= "Unable to execute sacct, with message: "+str(E)+"\n"
		ferr.close()
		fout.close()
		return {}, outMsg
	
	args = ['egrep', '-v', '\|00:00:00\||PENDING|.batch|bullatos']
	try:
		#"grep out" some unwanted lines
		subprocess.run(args, stdout=fout,stderr=ferr, stdin= p1.stdout)
		p1.stdout.close()
	except Exception as E:
		outMsg= "Unable to execute sacct, with message: "+str(E)+"\n"
		ferr.close()
		fout.close()
		return {}, outMsg

	fout.seek(0)
	ferr.seek(0)
	
	#verify if there was a result from the sacct command
	if os.path.getsize(fout.name) == 0:
		outMsg= "Error while executing sacc!\n"
		ferr.close()
		fout.close()
		return {}, outMsg
	
	#date fromat from sacct output
	date_format = "%Y-%m-%d %H:%M:%S"
		
	
	for line in fout.readlines():
		fieldLen= len(line.decode('utf-8').strip().split("|"))
		if fieldLen != 11:
			continue
		JobID, JobName, Account, Partition, NNodes, NCPUS, Start, End, Elapsed, State, NodeList= line.decode('utf-8').strip().split("|")
		
		#get the elapsed time in seconds
		try:
			elapsedDT= datetime.datetime.strptime(End, date_format) - datetime.datetime.strptime(Start, date_format) 
			Elapsed= str(int(elapsedDT.total_seconds()))
		except:
			Elapsed= "0"
		#skip jobs with wallclock under 2 minutes - they are usually test jobs
		if int(Elapsed) <= 120:
			continue
				
		Exec= "unknown"		
		#Default executed "applications" by this Groups
		if Account in ["dockvs", "dockthor"]:
			Exec= "DockThor"
		elif Account == "lhcb":
			Exec= "LHCB DIRAC"
		elif Account == "galphat":
			Exec="BIE"
		#if the groups isn't from any of those three, will try to find the executable name
		else:
			#need to requery the sacc with the job id to the job steps and search based on the "job name" field
			#the result will be based on the last line from the new query
			#arguments used to query the slurm jobs' database
			args = ['sacct', '-j', JobID, '-a', '-P', '-o JobID,JobName', '-n']
			
			#generate new temporary files to store sacct output
			foutJobStep= tempfile.TemporaryFile()
			ferrJobStep= tempfile.TemporaryFile()
			
			try:
				p1= subprocess.Popen(args, stdout=subprocess.PIPE, env=os.environ)
			except Exception as E:
				outMsg= outMsg+"Unable to execute secondary sacct (JobID "+JobID+"), with message: "+str(E)+"\n"
				ferrJobStep.close()
				foutJobStep.close()
				continue
        
			args = ['tail', '-1']
			try:
				#"grep out" some unwanted lines
				subprocess.run(args, stdout=foutJobStep,stderr=ferrJobStep, stdin= p1.stdout)
				p1.stdout.close()
			except Exception as E:
				outMsg= outMsg+"Unable to execute secondary sacct (JobID "+JobID+"), with message: "+str(E)+"\n"
				ferrJobStep.close()
				foutJobStep.close()
				continue

			foutJobStep.seek(0)
			ferrJobStep.seek(0)
			
			for lineJobStep in foutJobStep.readlines():
				JobStepJobID, JobStepJobName= lineJobStep.decode('utf-8').strip().split("|")
			ferrJobStep.close()
			foutJobStep.close()
			for appName, appBin in appNames.items():
				if  JobStepJobName in appBin:
					Exec= appName
					break
			print(JobStepJobID+" - "+Exec+" - "+JobStepJobName)
			#try to find the application name through the executable file name
			if Exec == "unknown":
				out = subprocess.run(["grep", "-w", JobStepJobName, execFile], universal_newlines=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
				if len(out.stdout) != 0:
					Exec= out.stdout.strip().split("|")[0]
			print(JobStepJobID+" - "+Exec+" - "+JobStepJobName)
		
		#end if to find the exec name
		
		#Get accout knowledge area
		
		KA= "unknown"
		try:
			out = subprocess.run(["grep", "-w", Account, KAFile], universal_newlines=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
			if len(out.stdout) != 0:
				KA= out.stdout.strip().split("|")[1]
		except Exception as E:
			outMsg= outMsg+"Error while reading Knowledge Area File: "+KAFile+", with message: "+str(E)+"\n"
		
		out = subprocess.run(["nodeset", "-e", "-S,", NodeList], universal_newlines=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
		NodeList= out.stdout.strip()
		jobsLst.append((JobID, Account, KA, JobName, Exec, NNodes, NCPUS, Start, End, Elapsed, State, NodeList))
		
		#ferr.close()
		#fout.close()
		#return {}
		
	ferr.close()
	fout.close()
	return jobsLst, outMsg

## MAIN part of the script

def main():
	jobsLst= []
	parser = argparse.ArgumentParser(description='Arguments')
	parser.add_argument('--start_date', "-s", required=False, metavar='YYYYMMDD', default= "", type=str, help='Specify the start date, in the format YYYYMMDD, to query sacct. If not specified, query current jobs.')
	parser.add_argument('--end_date', "-e", required=False, metavar='YYYYMMDD', default= "", type=str, help='Specify the end date, in the format YYYYMMDD, to query sacct. If not specified, query current jobs.')
	parser.add_argument('--ka_file', "-k", required=True, metavar='FILE', type=str, help='Specify Project+Knowledge Area file name to use')
	parser.add_argument('--exec_file', "-ef", required=True, metavar='FILE', type=str, help='Specify Application executable name file name to use')
	parser.add_argument('--file', "-f", required=True, metavar='FILE', type=str, help='Specify file name to use for the output')
	
	args = parser.parse_args()
	
	startDate= args.start_date[0:4]+"-"+args.start_date[4:6]+"-"+args.start_date[6:8]+" 00:00:00"
	endDate= args.end_date[0:4]+"-"+args.end_date[4:6]+"-"+args.end_date[6:8]+" 23:59:59"

	#verify if it's able to access the output file
	try:
		outFileHandle = open(args.file, 'a')
	except:
		sys.stderr.write("Unable to access output file "+args.file+"\n")
		return -1
	outFileHandle.close()

	#verify if it's able to access the Project+Knowledge Area file
	try:
		outFileHandle = open(args.ka_file, 'r')
	except:
		sys.stderr.write("Unable to access Project+Knowledge Area file "+args.ka_file+"\n")
		return -1
	outFileHandle.close()

	#verify if it's able to access the Application executable name file
	try:
		outFileHandle = open(args.exec_file, 'r')
	except:
		sys.stderr.write("Unable to access Application executable name file "+args.exec_file+"\n")
		return -1
	outFileHandle.close()

	jobsLst, outMsg= exec_sacct(startDate, endDate, args.ka_file, args.exec_file)

	#verify if sacct was executed successfully
	
	if (not jobsLst):
		sys.stderr.write(outMsg)
		return -1
	
	colNames= ['JobID', 'Account', 'KA', 'JobName', 'Exec', 'NNodes', 'NCPUS', 'Start', 'End', 'Elapsed', 'State', 'NodeList']
	outMsg= write_csv_dataset(args.file, jobsLst, colNames)
	if (outMsg):
		sys.stderr.write("Error while writing output file "+args.file+", with message: " + outMsg+"\n")
	
	return 0

if __name__ == "__main__":
	main()
