# Uncovering I/O Demands on HPC Platforms: Peeking Under the Hood of Santos Dumont

André Ramos Carneiro (1,3) , Jean Luca Bez (2) , Carla Osthoff (3) , Lucas Mello Schnorr (1) , Philippe O. A. Navaux (1)

(1) Institute of Informatics, Federal University of Rio Grande do Sul (UFRGS), Brazil. 
(2) Lawrence Berkeley National Laboratory (LBNL), USA. 
(3) National Laboratory of Scientific Computation (LNCC), Brazil. 

Companion repository for our JPDC Special Issue for SBAC-PAD 2021 paper. Contains all data and additional figures.

# Table of contents <a name="TOC"></a>


* [Figures](#Figures)
  * [Overview of Lustre Usage](#OverviewLustre)
    * [I/O Data Analysis](#IOData)
    * [Metadata Analysis](#Metadata)
  * [Detailed View of a Region of Interest](#Detailed)
    * [Applications I/O Data Analysis](#AppIOData)
    * [Applications Metadata Analysis](#AppMetadata)


# Figures <a name="Figures"></a>

## Overview of Lustre Usage <a name="OverviewLustre"></a>

### I/O Data Analysis <a name="IOData"></a>

![](./figures/2020/filesystem_read-write.png)

Figure 1: 2020 data usage distribution for 3 months. The *y*-axis is the Throughput in GiB aggregated by minute.

![](./figures/2021/filesystem_read-write_2021.png)

Figure 2: 2021 data usage distribution for 3 months. The *y*-axis is the Throughput in GiB aggregated by minute.

<br>

***

<br>

![](./figures/2020/cfd_throughput_size_v2.png)

Figure 3: 2020 *CDF* of the Operation Size (**A**) and Throughput (**B**) for the Read (Red) and Write (Blue) operations among OSTs. The vertical lines represent the mean observed values: $652$ KiB Read and $1729$ KiB Write for Op. Size, and $1.5$ GiB/m Read and $2.25$ GiB/m Write for Throughput.

![](./figures/2021/cfd_throughput_size_v2_2021.png)

Figure 4: 2021 *CDF* of the Operation Size (**A**) and Throughput (**B**) for the Read (Red) and Write (Blue) operations among OSTs. he vertical lines represent the mean observed values: $1018$ KiB Read and $1488$ KiB Write for Op. Size, and $5.47$ GiB/m Read and $3.58$ GiB/m Write for Throughput.

<br>

***

<br>

![](./figures/2020/workload_vol-ops_pALL_by_week-of-year-facet.png)

Figure 5: 2020 Workload distribution by week. Left facet is the Number of Operations performed and right facet is the Volume of Data transferred The *x*-axis represents the week of the year, starting at March 10th.

![](./figures/2021/workload_vol-ops_pALL_by_week-of-year-facet_2021.png)

Figure 6: 2021 Workload distribution by week. Left facet is the Number of Operations performed and right facet is the Volume of Data transferred The *x*-axis represents the week of the year, starting at March 1st.

<br>

***

<br>

![](./figures/2020/load_imbalance_read_x_write_pALL.png)

Figure 7: 2020 3 Hour Simple Moving Averages (*SMA_3HR*) of Load Imbalance metric for the read (red) and write (blue) workload. Values below $0.5$ can be considered as low imbalance, values around $1$ are considered as moderate, and values above are considered severe imbalance. The missing values are due to scheduled maintenance on the ClusterStor.

![](./figures/2021/load_imbalance_read_x_write_pALL_2021.png)

Figure 8: 2021 3 Hour Simple Moving Averages (*SMA_3HR*) of Load Imbalance metric for the read (red) and write (blue) workload. Values below $0.5$ can be considered as low imbalance, values around $1$ are considered as moderate, and values above are considered severe imbalance. The missing values are due to scheduled maintenance on the ClusterStor.

<br>

***

<br>

![](./figures/2020/sma_read+write_pALL_by-ost_v2.png)

Figure 9: 2020 3 Hour Simple Moving Averages (*SMA_3HR*) of read and write throughput by OST.

![](./figures/2021/sma_read+write_pALL_by-ost_v2_2021.png)

Figure 10: 2021 3 Hour Simple Moving Averages (*SMA_3HR*) of read and write throughput by OST.

<br>

***

<br>
[Table of contents](#TOC)

### Metadata Analysis <a name="Metadata"></a>

![](./figures/2020/metadata-io_load_by_week-of-year.png)

Figure 11: 2020 metadata load distribution by week. (**A**) depicts the load of I/O operations (purple) and and metadata operations (yellow). (**B**) details the metadata operation type. The *x*-axis is the week of the year and *y*-axis is the load (%).

![](./figures/2021/metadata-io_load_by_week-of-year_2021.png)

Figure 12: 2021  metadata load distribution by week. (**A**) depicts the load of I/O operations (purple) and and metadata operations (yellow). (**B**) details the metadata operation type. The *x*-axis is the week of the year and *y*-axis is the load (%).

<br>

***

<br>

![](./figures/2020/histogram_metadata.png)

Figure 11: 2020 histogram of metadata operations.

![](./figures/2021/histogram_metadata_2021.png)

Figure 12: 2021 histogram of metadata operations.

<br>

***

<br>
[Table of contents](#TOC)

## Detailed View of a Region of Interest <a name="Detailed"></a>

### Applications I/O Data Analysis <a name="AppIOData"></a>

![](./figures/2020/application_x_ka.png)

Figure 13: Identified applications in the 2020 dataset and their Science Domains. *Count* represent the number of executed jobs.

![](./figures/2021/application_x_ka_p3_2021.png)

Figure 14: Identified applications in the 2021 dataset and their Science Domains. *Count* represent the number of executed jobs.

<br>

***

<br>

![](./figures/2020/cfbw_timeseries_point.png)

Figure 15: 2020 Bandwidth Coverage Factor (CF_bw) of the jobs. The dots in red, black, and blue represent the Max., Avg. and Min., respectively, of all jobs, observed on each timestamp.

![](./figures/2021/cfbw_timeseries_point_p3_2021.png)

Figure 16: 2021 Bandwidth Coverage Factor (CF_bw) of the jobs. The dots in red, black, and blue represent the Max., Avg. and Min., respectively, of all jobs, observed on each timestamp.

<br>

***

<br>

![](./figures/2020/size_and_quality_distribution_by-application_facet.png)

Figure 17: 2020 Distribution of the Quality of Operation (left) and Transfer Size (right). The *x*-axis are the **QO** index and **size** in KiB, respectively.

![](./figures/2021/size_and_quality_distribution_by-application_facet_p3_2021.png)

Figure 18: 2021 Distribution of the Quality of Operation (left) and Transfer Size (right). The *x*-axis are the **QO** index and **size** in KiB, respectively.

<br>

***

<br>

![](./figures/2020/workload_by_application.png)

Figure 17: 2020 I/O workload by application.

![](./figures/2021/workload_by_application_p3_2021.png)

Figure 18: 2021 I/O workload by application.

<br>

***

<br>

![](./figures/2020/workload_by_ka.png)

Figure 17: 2020 I/O workload by Science Domain.

![](./figures/2021/workload_by_ka_p3_2021.png)

Figure 18: 2021 I/O workload by Science Domain.

<br>

***

<br>

![](./figures/2020/cdf_simultaneous_io_component_summary.png)

Figure 19: 2020 *CDF* of the simultaneous resources used during I/O activities by the applications at each timestamp. Vertical lines represent the average: $\approx3.24$ for OSTs used and $\approx1.64$. The maximum simultaneous OST used is $10$.

![](./figures/2021/cdf_simultaneous_io_component_summary_2021_p3.png)

Figure 20: 2021 *CDF* of the simultaneous resources used during I/O activities by the applications at each timestamp. Vertical lines represent the average: $\approx3.6$ for OSTs used and $\approx1.65$. The maximum simultaneous OST used is $10$.

<br>

***

<br>

![](./figures/2020/simultaneous_io_component_boxplot.png)

Figure 21: 2020 Distribution of the Simultaneous Resource Used by each application in read (red) and write (blue). The *y*-axis (count) represents the amount of resource simultaneously used by each job of the application.

![](./figures/2021/simultaneous_io_component_boxplot_2021_p3.png)

Figure 22: 2021 Distribution of the Simultaneous Resource Used by each application in read (red) and write (blue). The *y*-axis (count) represents the amount of resource simultaneously used by each job of the application.

<br>

***

<br>

[Table of contents](#TOC)

### Applications Metadata Analysis <a name="AppMetadata"></a>

![](./figures/2020/application_metadata_operations_v1.png)

Figure 23: 2020 applications' metadata load distribution. (**A**) presents the load division between I/O (purple) and and metadata operations (yellow). (**B**) presents the division among each metadata operation type. The *x*-axis is the application's name and *y*-axis is the load (%).

![](./figures/2021/application_metadata_operations_2021_v1.png)

Figure 24: 2021 applications' metadata load distribution. (**A**) presents the load division between I/O (purple) and and metadata operations (yellow). (**B**) presents the division among each metadata operation type. The *x*-axis is the application's name and *y*-axis is the load (%).

<br>

***

<br>


![](./figures/2020/ka_metadata_operations_v1.png)

Figure 25: 2020 Science Domains metadata load distribution. (**A**) presents the load division between I/O (purple) and and metadata operations (yellow). (**B**) presents the division among each metadata operation type. The *x*-axis is the application's name and *y*-axis is the load (%).

![](./figures/2021/ka_metadata_operations_2021_v1.png)

Figure 26: 2021 Science Domains metadata load distribution. (**A**) presents the load division between I/O (purple) and and metadata operations (yellow). (**B**) presents the division among each metadata operation type. The *x*-axis is the application's name and *y*-axis is the load (%).

<br>

***

<br>

[Table of contents](#TOC)
